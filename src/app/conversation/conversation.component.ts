import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {
  friendId: any;
  friends: User[];
  friend: User;
  price: number = 78.2313132313;
  today: any = Date.now();
  constructor(private activateRoute: ActivatedRoute,
              private userService: UserService) { 
    this.friends = this.userService.getFriends();
    this.friendId=this.activateRoute.snapshot.params['uid'];
    this.friend = this.friends.find((record)=>{
      return record.uid == this.friendId;
    })
  }

  ngOnInit() {
  }

}
